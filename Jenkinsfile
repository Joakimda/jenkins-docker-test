pipeline {
    agent any
    stages {
        stage('Checkout') {
            steps {
                script {
                    checkout([
                            $class:           'GitSCM',
                            branches:          scm.branches,
                            extensions:        scm.extensions + [[$class: 'CleanCheckout'], [$class: 'LocalBranch', localBranch: '**']],
                            userRemoteConfigs: scm.userRemoteConfigs
                    ])
                    lastCommitMessage = sh(script: "git log --pretty=format:'%s' | head -n 1",  returnStdout: true)
                
                    env.DEV_REPO = 'testpurpose-docker-local.jfrog.io'
                    env.ARTIFACT_ID = 'shared-rides'
                    env.ARTIFACT_VERSION = env.BUILD_ID
                    env.ARTIFACT_RC_VERSION = "RC-${ARTIFACT_VERSION}"

                    println "Build ${ARTIFACT_ID} of version ${ARTIFACT_RC_VERSION}"
                }
            }
        }
        stage('Build') {
            stages {
                stage('Install Dependencies') {
                    steps {
                        script {
                            sh 'npm install'
                        }
                    }
                }
                stage('Tests') {
                    steps {
                        script {
                            sh 'npm run test'
                        }
                    }
                }
                stage('Release Candidate To Artifactory') {
                    steps {
                        script {
                            sh "docker build -t $DEV_REPO/$ARTIFACT_ID:$ARTIFACT_RC_VERSION ."
                            
                            // Push release candidate and latest docker images
                            withCredentials([usernamePassword(credentialsId: "${env.JENKINS_TOKEN}",
                                    usernameVariable: 'USERNAME',
                                    passwordVariable: 'PASSWORD')]) {

                            sh "docker login $DEV_REPO -u ${env.USER} -p ${env.PASSWORD}"
                            sh "docker push $DEV_REPO/$ARTIFACT_ID:$ARTIFACT_RC_VERSION"
                            sh "docker push $DEV_REPO/$ARTIFACT_ID:latest"
                            sh "docker logout $DEV_REPO"
                            }
                        }
                    }
                }
                stage('Release?') {
                    // when {
                    //     branch 'master'
                    //     beforeAgent true
                    // }
                    stages {
                        stage ('Approve Release') {
                            steps {
                                script {
                                    try {
                                        timeout(time: 2, unit: 'MINUTES') {
                                            env.PERFORM_RELEASE = input message: 'Release artifact?',
                                                    parameters: [choice(name: 'Perform release', choices: ['Yes', 'No'], description: 'Perform release')]
                                        }
                                    } catch (err) {
                                        echo "Release aborted"
                                        env.PERFORM_RELEASE = 'No'
                                    }
                                }
                                echo "Release: ${PERFORM_RELEASE}"
                            }
                        }
                    }
                }
                stage('Release to Artifactory') {
                    when {
                        environment ignoreCase: true, name: 'PERFORM_RELEASE', value: 'Yes'
                    }
                    steps {
                        script {
                            // Re-tag to release version and latest docker images
                            sh "docker tag $DEV_REPO/$ARTIFACT_ID:$ARTIFACT_RC_VERSION $DEV_REPO/$ARTIFACT_ID:$ARTIFACT_VERSION"
                            sh "docker tag $DEV_REPO/$ARTIFACT_ID:$ARTIFACT_RC_VERSION $DEV_REPO/$ARTIFACT_ID:latest"

                                // Push release and latest docker images
                                withCredentials([usernamePassword(credentialsId: "${env.JENKINS_TOKEN}",
                                        usernameVariable: 'USERNAME',
                                        passwordVariable: 'PASSWORD')]) {
                                            
                                sh "docker login $DEV_REPO -u ${env.USER} -p ${env.PASSWORD}"
                                sh "docker push $DEV_REPO/$ARTIFACT_ID:$ARTIFACT_VERSION"
                                sh "docker push $DEV_REPO/$ARTIFACT_ID:latest"
                                sh "docker logout $DEV_REPO"
                            }
                            slackSend (color: '#008000', message: "Artifact released successfully: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
                        }  
                    }
                }
            }
        }
    }
}